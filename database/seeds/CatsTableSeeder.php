<?php

use Illuminate\Database\Seeder;

class CatsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        DB::table('cats')->insert([
            ['label'=> "Pop" ],
            ['label'=> "Rock" ],
            ['label'=> "Jazz" ],
            ['label'=> "Classical" ]
        ]);
    }
}
