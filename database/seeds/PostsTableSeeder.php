<?php

use Illuminate\Database\Seeder;

class PostsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('posts')->insert([
            [
                'file'=>'mike.jpeg',
                'type'=> false,//image
                'caption'=>'Michael Jackson',
                'cat_id'=> 1 //Pop
            ],
            [
                'file'=>'marilyn.jpeg',
                'type'=> false,//image
                'caption'=>'Marilyn Manson',
                'cat_id'=> 2 //Rock
            ],
            [
                'file'=>'frank.jpeg',
                'type'=> false,//image
                'caption'=>'Frank Sinatra',
                'cat_id'=> 16 //Jazz
            ],
            [
                'file'=>'mozart.jpeg',
                'type'=> false,//image
                'caption'=>'Wolfgang Mozart',
                'cat_id'=> 4 // classical
            ],
            [
                'file'=>'taylor.jpeg',
                'type'=> false,//image
                'caption'=>'Taylor Swift',
                'cat_id'=> 1
            ],
            [
                'file'=>'ed.jpeg',
                'type'=> false,//image
                'caption'=>'Ed Sheeran',
                'cat_id'=> 1
            ],
            [
                'file'=>'louis.jpeg',
                'type'=> false,//image
                'caption'=>'Louis Armstrong',
                'cat_id'=> 16
            ],
            [
                'file'=>'chopin.jpeg',
                'type'=> false,//image
                'caption'=>'Frederic Chopin',
                'cat_id'=> 4
            ],
            [
                'file'=>'led.jpeg',
                'type'=> false,//image
                'caption'=>'Led Zeppelin',
                'cat_id'=> 2
            ]            
        ]);
    }
}
