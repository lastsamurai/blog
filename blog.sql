-- MySQL dump 10.16  Distrib 10.1.38-MariaDB, for Linux (x86_64)
--
-- Host: localhost    Database: blog
-- ------------------------------------------------------
-- Server version	10.1.38-MariaDB

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `cats`
--

DROP TABLE IF EXISTS `cats`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cats` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `label` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cats`
--

LOCK TABLES `cats` WRITE;
/*!40000 ALTER TABLE `cats` DISABLE KEYS */;
INSERT INTO `cats` VALUES (1,'Pop',NULL,NULL),(2,'Rock',NULL,NULL),(3,'Jazz',NULL,NULL),(4,'Classical',NULL,NULL);
/*!40000 ALTER TABLE `cats` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `migrations`
--

DROP TABLE IF EXISTS `migrations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `migrations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `migrations`
--

LOCK TABLES `migrations` WRITE;
/*!40000 ALTER TABLE `migrations` DISABLE KEYS */;
INSERT INTO `migrations` VALUES (1,'2014_10_12_000000_create_users_table',1),(2,'2019_04_30_091133_add_token_to_user',1),(3,'2019_04_30_093837_create_otp_table',2),(4,'2019_05_01_100744_create_cats_table',3),(5,'2019_05_01_100752_create_posts_table',3),(6,'2019_05_01_103042_create_post_user_table',4);
/*!40000 ALTER TABLE `migrations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `otps`
--

DROP TABLE IF EXISTS `otps`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `otps` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `otp` varchar(6) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` bigint(20) unsigned NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=40 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `otps`
--

LOCK TABLES `otps` WRITE;
/*!40000 ALTER TABLE `otps` DISABLE KEYS */;
INSERT INTO `otps` VALUES (1,'888888',3,NULL,NULL),(2,'198191',6,'2019-05-01 02:01:44','2019-05-01 02:01:44'),(3,'606315',6,'2019-05-01 02:01:55','2019-05-01 02:01:55'),(4,'891226',6,'2019-05-01 02:01:55','2019-05-01 02:01:55'),(5,'333720',6,'2019-05-01 02:01:56','2019-05-01 02:01:56'),(6,'964701',6,'2019-05-01 02:01:56','2019-05-01 02:01:56'),(7,'194505',3,'2019-05-01 02:02:54','2019-05-01 02:02:54'),(8,'994609',3,'2019-05-01 02:03:14','2019-05-01 02:03:14'),(9,'828860',3,'2019-05-01 02:03:25','2019-05-01 02:03:25'),(10,'934402',3,'2019-05-01 02:03:37','2019-05-01 02:03:37'),(11,'247549',3,'2019-05-01 02:03:37','2019-05-01 02:03:37'),(12,'945138',3,'2019-05-01 02:03:38','2019-05-01 02:03:38'),(13,'102525',7,'2019-05-01 03:05:27','2019-05-01 03:05:27'),(15,'645337',7,'2019-05-01 03:31:24','2019-05-01 03:31:24'),(16,'306942',7,'2019-05-01 03:34:17','2019-05-01 03:34:17'),(17,'157773',7,'2019-05-01 03:50:03','2019-05-01 03:50:03'),(18,'909457',7,'2019-05-01 03:55:22','2019-05-01 03:55:22'),(19,'243193',7,'2019-05-01 03:57:42','2019-05-01 03:57:42'),(20,'499953',7,'2019-05-01 03:59:34','2019-05-01 03:59:34'),(21,'360090',7,'2019-05-01 04:01:24','2019-05-01 04:01:24'),(22,'169298',7,'2019-05-01 04:03:41','2019-05-01 04:03:41'),(23,'874699',7,'2019-05-01 04:07:01','2019-05-01 04:07:01'),(24,'961373',7,'2019-05-01 04:08:50','2019-05-01 04:08:50'),(25,'961700',3,'2019-05-01 05:23:55','2019-05-01 05:23:55'),(26,'735520',3,'2019-05-01 05:23:57','2019-05-01 05:23:57'),(27,'326617',3,'2019-05-01 05:25:11','2019-05-01 05:25:11'),(28,'836570',8,'2019-05-01 08:16:08','2019-05-01 08:16:08'),(29,'889728',8,'2019-05-01 08:17:39','2019-05-01 08:17:39'),(30,'228471',9,'2019-05-05 01:17:32','2019-05-05 01:17:32'),(31,'955607',3,'2019-05-05 06:16:44','2019-05-05 06:16:44'),(32,'727024',3,'2019-05-05 06:33:42','2019-05-05 06:33:42'),(33,'499509',3,'2019-05-05 06:39:02','2019-05-05 06:39:02'),(34,'931860',3,'2019-05-05 06:43:07','2019-05-05 06:43:07'),(35,'985631',3,'2019-05-05 06:45:17','2019-05-05 06:45:17'),(36,'401219',23,'2019-05-05 06:51:12','2019-05-05 06:51:12'),(37,'157645',3,'2019-05-05 06:51:31','2019-05-05 06:51:31'),(38,'466032',24,'2019-05-05 06:55:55','2019-05-05 06:55:55'),(39,'852284',3,'2019-05-05 06:56:11','2019-05-05 06:56:11');
/*!40000 ALTER TABLE `otps` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `post_user`
--

DROP TABLE IF EXISTS `post_user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `post_user` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `post_id` bigint(20) unsigned NOT NULL,
  `user_id` bigint(20) unsigned NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `post_user_post_id_foreign` (`post_id`),
  KEY `post_user_user_id_foreign` (`user_id`),
  CONSTRAINT `post_user_post_id_foreign` FOREIGN KEY (`post_id`) REFERENCES `posts` (`id`),
  CONSTRAINT `post_user_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `post_user`
--

LOCK TABLES `post_user` WRITE;
/*!40000 ALTER TABLE `post_user` DISABLE KEYS */;
INSERT INTO `post_user` VALUES (1,3,7,NULL,NULL),(2,3,7,NULL,NULL),(3,3,7,NULL,NULL),(4,3,7,NULL,NULL),(5,3,7,NULL,NULL),(6,1,8,NULL,NULL),(7,3,8,NULL,NULL),(8,3,8,NULL,NULL),(9,3,8,NULL,NULL),(10,3,8,NULL,NULL),(11,3,8,NULL,NULL),(12,3,8,NULL,NULL),(13,3,8,NULL,NULL);
/*!40000 ALTER TABLE `post_user` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `posts`
--

DROP TABLE IF EXISTS `posts`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `posts` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `cat_id` bigint(20) unsigned NOT NULL,
  `file` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `type` tinyint(1) NOT NULL,
  `caption` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `posts_cat_id_foreign` (`cat_id`),
  CONSTRAINT `posts_cat_id_foreign` FOREIGN KEY (`cat_id`) REFERENCES `cats` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `posts`
--

LOCK TABLES `posts` WRITE;
/*!40000 ALTER TABLE `posts` DISABLE KEYS */;
INSERT INTO `posts` VALUES (1,1,'mike.jpeg',0,'Michael Jackson',NULL,NULL),(2,2,'marilyn.jpeg',0,'Marilyn Manson',NULL,NULL),(3,3,'frank.jpeg',0,'Frank Sinatra',NULL,NULL),(4,4,'mozart.jpeg',0,'Wolfgang Mozart',NULL,NULL),(5,1,'taylor.jpeg',0,'Taylor Swift',NULL,NULL),(6,1,'ed.jpeg',0,'Ed Sheeran',NULL,NULL),(7,3,'louis.jpeg',0,'Louis Armstrong',NULL,NULL),(8,4,'chopin.jpeg',0,'Frederic Chopin',NULL,NULL),(9,2,'led.jpeg',0,'Led Zeppelin',NULL,NULL);
/*!40000 ALTER TABLE `posts` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `api_token` varchar(80) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`),
  UNIQUE KEY `users_api_token_unique` (`api_token`)
) ENGINE=InnoDB AUTO_INCREMENT=25 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (3,'Hadi','caffotinated@gmail.com',NULL,'2019-04-30 09:24:59','2019-05-05 06:56:12','$2y$10$BsgKctDlmninuzpemjY3oeYlRoWcDJjETKwj12OeplqseZYuSiv6m'),(4,'Hadiiii','smh.mirsadeghi@gmail.com',NULL,'2019-04-30 09:42:01','2019-04-30 09:42:01',NULL),(5,'Michael','michael@jackson.com',NULL,'2019-05-01 01:10:58','2019-05-01 01:10:58',NULL),(6,'Michael','frank@sinatra.com',NULL,'2019-05-01 01:13:50','2019-05-01 01:13:50',NULL),(7,'lastsamurai','h.mir@iasbs.ac.ir',NULL,'2019-05-01 03:05:12','2019-05-01 04:08:59','$2y$10$atuSOx3fYyc6L/6VqS8G2eAE/y68MNgc88o2CP2pG3Q2gRv88.t5O'),(8,'Ramin','ramin@ramin.com',NULL,'2019-05-01 08:15:57','2019-05-01 08:17:47','$2y$10$6P/iF6AxsqyOIsm7QCmHdehoLT.PeFjnmuC3qHr67J5hgJAP5VCP6'),(9,'Dan','dan@danny.com',NULL,'2019-05-05 01:17:14','2019-05-05 01:18:00','$2y$10$6VFi0JrhQfSUG9/VU8uaSuJkZU.blwV7MBfGxNpf3adaNn0cN1EFC'),(10,'Hadi','mir@mir.com',NULL,'2019-05-05 04:56:49','2019-05-05 04:56:49',NULL),(11,'ramin','raminasdf@asfdas.com',NULL,'2019-05-05 05:16:51','2019-05-05 05:16:51',NULL),(12,'asdf','sadlkfa@asdf.com',NULL,'2019-05-05 05:26:23','2019-05-05 05:26:23',NULL),(13,'asdf','adjgf@asd.com',NULL,'2019-05-05 05:27:29','2019-05-05 05:27:29',NULL),(14,'Hadi','aslkdf@sadf.com',NULL,'2019-05-05 05:29:30','2019-05-05 05:29:30',NULL),(15,'asdf','sadf@gas.com',NULL,'2019-05-05 05:32:02','2019-05-05 05:32:02',NULL),(16,'asdf2','asdfa@gas.co',NULL,'2019-05-05 05:32:26','2019-05-05 05:32:26',NULL),(17,'asdfg','sadfj@gasfd.com',NULL,'2019-05-05 05:33:35','2019-05-05 05:33:35',NULL),(18,'asdf','sfdjka@asdf.com',NULL,'2019-05-05 06:08:35','2019-05-05 06:08:35',NULL),(19,'hadi','hasdf@fa.com',NULL,'2019-05-05 06:09:00','2019-05-05 06:09:00',NULL),(20,'hadi','asdfas@ad.com',NULL,'2019-05-05 06:13:56','2019-05-05 06:13:56',NULL),(21,'emad','asdf@sadf.com',NULL,'2019-05-05 06:15:22','2019-05-05 06:15:22',NULL),(22,'ahdi','asdfasfdf@asfdasf.com',NULL,'2019-05-05 06:50:26','2019-05-05 06:50:26',NULL),(23,'Hadi','hadihadi@hadi.chad',NULL,'2019-05-05 06:51:12','2019-05-05 06:51:15','$2y$10$EoaE6I.n5SULjojoJGCcQOmO0zvUT7hgTqUEMu4zM.lCn3LWSsUf2'),(24,'hadi','hadi@hadi.hadi',NULL,'2019-05-05 06:55:55','2019-05-05 06:56:01','$2y$10$60cFmn3zcgDnw7T.xISwmOc2zXaaIntjPTE8WqCrI8bzbQuPyrUjm');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2019-05-05 16:01:19
