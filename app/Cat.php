<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Cat extends Model
{
    protected $fillable=['label'];

    public function posts(){
        return $this->hasMany('\App\Post');
    }

}
