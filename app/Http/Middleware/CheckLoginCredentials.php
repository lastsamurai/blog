<?php

namespace App\Http\Middleware;
use Validator;
use Closure;
use App\Otp;use App\User;

class CheckLoginCredentials
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $val=Validator::make($request->all(),[
            'email' => 'required|email|exists:users,email',
            'otp' => 'required|digits:6|exists:otps,otp'
        ]);
        if($val->fails()){
            $response=[
                'meta'=>[
                        'error'=>$val->errors(),
                        'message'=>trans('messages.not_valid_input')
                ],
                'status'=>false
            ];
            return response($response);
        }
        
        $user=User::where('email',$request->email)->get()->first();
        $otp=$user->otps->first();

        if($otp->otp != $request->otp)
            return response([
                'meta'=>[
                    'error'=>"Invalid OTP",
                    'message'=>"otp is invalid or obsolete"
                ],
                'status'=>false
            ]);

        if($otp->hasExpired())
            return response([
                'meta'=>[
                    'error'=>"OTP has Expired",
                    'message'=>"try getting another otp"
                ],
                'status'=>false
            ]);
        
        return $next($request);
    }
}
