<?php

namespace App\Http\Middleware;
use Validator;
use Closure;
use App\User;

class CheckAuth
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $val=Validator::make($request->all(),[
            'token'=>'required|exists:users,api_token'
        ]);
        if($val->fails()){
            $response=[
                'meta'=>[
                        'error'=>$val->errors(),
                        'message'=>trans('messages.not_valid_input')
                ],
                'data'=>null
            ];
            return response($response);
        }

        $user=User::where('api_token',$request->token)->get()->first();

        $request->attributes->add(['user'=>$user]);

        return $next($request);
    }
}
