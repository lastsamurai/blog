<?php

namespace App\Http\Middleware;
use Validator;
use Closure;

class CheckRegistrationForm
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $val=Validator::make($request->all(),[
            'name' => 'required|max:255',
            'email' => 'required|email|unique:users,email'
        ]);
        if($val->fails()){
            $response=[
                'meta'=>[
                        'error'=>$val->errors(),
                        'message'=>trans('messages.not_valid_input')
                ],
                'status'=>false
            ];
            return response($response);
        }
        
        return $next($request);
    }
}
