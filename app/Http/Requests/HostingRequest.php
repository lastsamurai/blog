<?php

namespace App\Http\Requests;
use App\Post;
use Illuminate\Foundation\Http\FormRequest;

class HostingRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        if(file_exists( public_path() . '/storage/' . $this->route('file') ) && 
        Post::where('file',$this->route('file'))->get()->first() )
            return true;
        return false;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            //
        ];
    }
}
