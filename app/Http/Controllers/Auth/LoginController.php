<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use App\User;use App\Otp;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }   


    public function register(Request $req){      
        $user=User::create([
            'name'=>$req->name,
            'email'=>$req->email
        ]);
        return response()->json([
            'id'=>$user->id,
            'name'=>$user->name,
            'email'=>$user->email,
            'status'=> true
        ]);
    }

    public function getOTP(Request $req){
        $user=User::where('email',$req->email)->get(['id','email'])->first();
        do{
            $random=mt_rand(100000,999999);
        }while( Otp::where('otp',$random)->get()->first() !=NULL );
        
        
        $p=Otp::create([
            'otp'=> $random,
            'user_id'=> $user->id
        ]);
        
        return [
            'email' => $user->email,
            'otp'=> $p->otp,
            'expiration'=> $p->created_at,
            'status' => true
        ];
    }

    public function login(Request $req){
        
        $user=User::where('email',$req->email)->get()->first();

        do {
            $random=str_random(10);
            $token=Hash::make(($req->email).$random);

        }while(User::where('api_token',$token)->get()->first() != NULL);

        $user->api_token=$token;
        $user->save();

        return [
            'token'=>$token,
            'status'=>true
        ];
    }

}

