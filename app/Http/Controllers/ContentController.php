<?php

namespace App\Http\Controllers;
use App\Cat;use App\Post;
use Illuminate\Http\Request;
use App\Http\Requests\CatRequest;
use App\Http\Requests\PostRequest;
use App\Http\Requests\HostingRequest;
use Plank\Mediable;
use MediaUploader;

class ContentController extends Controller
{
    public function listCats(){
        $cats=Cat::all('id','label');
        return $cats->toJson(JSON_PRETTY_PRINT);
    }

    public function createCat(Request $req){

        $label=$req->request->get('label');
        $cat=new Cat();
        $cat->label=$label;

        $cat->save();
        

        return "k";
    }

    public function deleteCat($id){
        
        $cat=Cat::find($id);
        $cat->delete();
        
        return "";
    }

    public function updateCat(Request $req,$id){
        $label=$req->request->get('label');
        $cat=Cat::find($id);
        $cat->label=$label;
        $cat->save();
        return "";
    }

    public function getPosts(CatRequest $req,$cid){
        $cats=Cat::all();
        $cat=Cat::find($cid);
        $posts=$cat->posts;
        foreach($posts as $post)
            if($post->file= $post->firstMedia('thumbnail'))$post->file= $post->firstMedia('thumbnail')->getUrl();
        $data=[
            "cat"=>[
                "label"=>$cat->label,
                "id"=>$cid
            ],
            "cats"=>$cats,
            "posts"=>$posts
        ];
        return $data;
    }

    public function viewPost(PostRequest $req,$pid){
        $post=Post::withCount('users')->find($pid);
        $post->users()->attach( $req->get('user') );
        $post->save();
        $file=$post->firstMedia('thumbnail');
        return [
            'id'=> $post->id,
            'cat'=>$post->cat,
            /*'file'=> route('host',['file'=>$post->file]),*/
            'file'=> $file?$file->getUrl():"NOFILE",
            'caption' => $post->caption,
            'views'=>$post->users_count
        ];
    }
    
    public function host(HostingRequest $req,$file){
        $post=Post::withCount('users')->where('file',$file)->get()->first();
        
        return view('post')
            ->with('file',$file)
            ->with('caption',$post->caption)
            ->with('views',$post->users_count);
    }

    public function deletePost($pid){
        
        $post=Post::find($pid);
        $post->delete();
        return "okay";
    }
    
    public function editPost(Request $req){
        $caption=$req->request->get('caption');
        $pid=$req->request->get('post_id');
        
        $post=Post::find($pid);
        
        $post->caption=$caption;
        
        $post->save();

        return "okay";
    }

    public function createPost(Request $req){
        
        $caption=$req->request->get('caption');
        $cid=$req->request->get('cat_id');
        $file=$req->request->get('contents');

        //use MEDIABLE
        $media = MediaUploader::fromSource($file)
        ->toDestination('public', 'blog/posts')
        ->upload();
        
        $post=new Post();
        $post->caption=$caption;
        $post->file="nofile";
        $post->cat_id=$cid;
        $post->save();
        $post->attachMedia($media, ['thumbnail']);

        
        return "okay";

        /*return dd($req->request);

        $file_name=md5( rand() );

        $flag=move_uploaded_file($file_tmp,"public/".$file_name);

        return new Response($flag);



        $fileUploader=new FileUploader();
        
        $file=$req->files->get('postFile');
        
        $fileName = $fileUploader->upload($file);
        
        $filename=$file->getClientOriginalName();
        //$uploader->upload($uploadDir, $file, $filename);
        return new Response(dd($filename));*/


    }
}
