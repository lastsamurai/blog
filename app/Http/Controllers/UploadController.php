<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Service\FileUploader;

class UploadController extends Controller
{
    public function index(Request $req,string $uploadDir,FileUploader $uploader){
        $file = $request->files->get('myfile');

        if (empty($file)) 
        {
            return new Response("No file specified",  
               Response::HTTP_UNPROCESSABLE_ENTITY, ['content-type' => 'text/plain']);
        }        

        $filename = $file->getClientOriginalName();
        $uploader->upload($uploadDir, $file, $filename);
    }
}
