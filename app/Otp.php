<?php

namespace App;
use Carbon\Carbon;

use Illuminate\Database\Eloquent\Model;

class Otp extends Model
{
    protected $fillable=['otp','user_id'];
    public $timestamps=true;
    public static $EXPIRATION_LIMIT=5;//in minutes

    public function hasExpired(){
        if( Carbon::now()->subMinutes($this->OTP_expiration_limit)->lt( $this->created_at ) )
            return false;
        return true;
    }

    public function user(){
        return $this->belongsTo('App\User');
    }
}
