<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Plank\Mediable\Mediable;
use App\Mediable as MediableModel;

class Post extends Model
{
    use Mediable;

    public function __construct(){
        $this->type=0;
    }
    protected $fillable=[
        'cat_id',
        'file',
        'type',
        'caption'
    ];

    public function cat(){
        return $this->belongsTo('\App\Cat');
    }

    public function users(){
        return $this->belongsToMany('\App\User');
    }
}
