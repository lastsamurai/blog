<?php

use Illuminate\Http\Request;
use App\Http\Middleware;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
//Login Routes
Route::post('/register','Auth\LoginController@register')->middleware(CheckRegistrationForm::class);
Route::get('/getOTP','Auth\LoginController@getOTP')->middleware(CheckRegisteredEmail::class);
Route::post('/login','Auth\LoginController@login')->middleware(CheckLoginCredentials::class);

//CRUD routes
Route::get('/cats','ContentController@listCats')->middleware(CheckAuth::class);
Route::post('/cat','ContentController@createCat')->middleware(CheckAuth::class); //middlware to check input

Route::get('/cats/{cid}','ContentController@getPosts')->middleware(CheckAuth::class);
Route::put('/cats/{cid}','ContentController@updateCat')->middleware(CheckAuth::class);
Route::delete('/cats/{cid}','ContentController@deleteCat')->middleware(CheckAuth::class);

Route::get('/posts/{pid}','ContentController@viewPost')->middleware(CheckAuth::class);
Route::post('/posts','ContentController@createPost')->name('fileUploadRoute');
Route::put('/posts','ContentController@editPost')->middleware(CheckAuth::class);
Route::delete('/posts/{pid}','ContentController@deletePost')->middleware(CheckAuth::class);

//Route::get('hosting/{file}','ContentController@host')->name('host')->middleware(CheckAuth::class);

